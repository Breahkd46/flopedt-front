#!/usr/bin/make -f

-include global.env

.PHONY: config update-config load-data start rm stop

PROJECT_NAME=front_flopedt_$(PORT)

config:
	$(MAKE) -C FlOpEDT config
	cp FlOpEDT/global.env global.env

update-config:
	cp global.env FlOpEDT/global.env

start:
	cd FlOpEDT && docker-compose \
		-p $(PROJECT_NAME) \
		-f docker-compose.$(CONFIG).yml \
		-f ../docker-compose.$(CONFIG).yml up -d

load-data:
	cd FlOpEDT && docker-compose \
    		-p $(PROJECT_NAME) \
    		-f docker-compose.$(CONFIG).yml \
    		-f ../docker-compose.$(CONFIG).yml \
		run --rm \
			-e BRANCH \
			-e DJANGO_LOADDATA=on \
			-e START_SERVER=off \
			web

debug:
	@echo CONFIG : $(CONFIG)
	@echo PROJECT : $(PROJECT_NAME)

rm:
	cd FlOpEDT && docker-compose -p $(PROJECT_NAME) \
    		-f docker-compose.$(CONFIG).yml -f ../docker-compose.$(CONFIG).yml rm

stop:
	cd FlOpEDT && docker-compose -p $(PROJECT_NAME) \
		-f docker-compose.$(CONFIG).yml -f ../docker-compose.$(CONFIG).yml stop
