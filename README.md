# flopedt-front

`flopedt-front` is one of front of the FlOpEDT project.
The project integrate FlOpEDT API as a git submodule.

## Requierements
- git
- make
- docker
- docker-compose

> If docker (or docker-compose) is only available to the superuser. 
>Try every `make` command by `sudo`.

## Project setup
TO clone the project.
```
git clone https://framagit.org/Breahkd46/flopedt-front.git
git submodule init
git submodule update
```

> If you want use ssh key replace `git clone https://framagit.org/Breahkd46/flopedt-front.git`
> by `git clone git@framagit.org:Breahkd46/flopedt-front.git`

> Another way, to clone the project : 
>```
>git clone --recurse-submodules -b develop https://framagit.org/Breahkd46/flopedt-front.git
>```

To update the FlOpEDT project :
```
git submodule update
``` 

### Use the make file to init and start the project service 
This project makefile is based on the FlOpEDT project's makefile.

The project launch works with docker.

#### Init
Generate config file:
```
make config
```
> This command must be run before every following command.

Update config for all projects:
```
make update-config
```
Run this command after every modification of the `./global.env` file.

#### Load data for tests
Load data:
```
make load-data
```
> Admin user:
> - Id : `MOI`
> - Password : `passe`

#### Start the project

```
make start
```
> This command will start the docker stack with following services: 
> - front-flopedt-8000_db -> PosgreSQL Database
> - front-flopedt-8000_redis -> NoSQL Database
> - front-flopedt-8000_web -> API
> - front-flopedt-8000_front -> Front
>
> If the variable in global.env PORT=8000

### Stop and Remove the project service 

#### Stop
```
make stop
```

#### Remove
```
make rm
```
> This command will remove the docker stack.
## Vue js Project set up
### Install dependencies
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Use Vue on IntelliJ
#### Set webpack for Vue

Go to `Settings` > `Languages and Frameworks` > 
`Javascript` > `Webpack`.


Choose a file in the project:
`node_modules/@vue/cli-service/webpack.config.js` 