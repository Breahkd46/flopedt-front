
const EdtFilterPlugin = {}


EdtFilterPlugin.install = (Vue, ) => {

    Vue.filter('time', (time) => {
        try {
            time = Number(time)
        } catch (e) {
            return time
        }
        const t = time / 60
        let dec = (t - Math.trunc(t)) * 60
        if (dec.toString().length === 1) dec = '0' + dec
        return Math.trunc(t) + 'h' + dec
    })
}

export default EdtFilterPlugin
