
export const lastChildren = (group) => {
    if (group.children === undefined) {
        return [group]
    }
    return group.children.reduce((acc, current) => {
        return acc.concat(lastChildren(current))
    }, [])
}

/**
 *
 * @param groups {[]}
 * @return {*}
 */
export const allSubGroup = (groups) => {
    return groups.reduce((acc, current) => {
        if (current.children === undefined) {
            return [current.name]
        }
        return acc.concat(lastChildren(current))
    }, [])
}

/**
 *
 * @param promoTree {Object}
 * @param events {Object[]}
 */
export const buildTree = (promoTree, events) => {
    const tree = {}
    tree.name = promoTree.name
    tree.enabled = promoTree.enabled
    tree.events = events.filter(value => value.course.group.name === promoTree.name)
    if (promoTree.children !== undefined) {
        tree.children = promoTree.children.map(value => buildTree(value, events))
    }
    return tree
}

/**
 *
 * @param promoTree {Object}
 * @param status {Boolean}
 */
export const setStateTree = (promoTree, status) => {
    const tree = {}
    tree.name = promoTree.name
    tree.enabled = status
    if (promoTree.children !== undefined) {
        tree.children = promoTree.children.map(child => setStateTree(child, status))
    }
    return tree
}

export const buildStateTree = (group) => {
    const tree = {}
    tree.name = group.name
    tree.enabled = group.enabled
    if (group.children !== undefined) {
        tree.children = group.children.map(child => buildStateTree(child))
    }
    return tree
}

export const buildGroupSelected = (promos, groups) => {
    const tree = {}
    promos.forEach(promo => {
        tree[promo] = buildStateTree(groups[promo])
    })
    return tree
}
