
export const url = (url, params) => {
    const _url = new URL(url)
    Object.keys(params).forEach(key => _url.searchParams.append(key, params[key]))
    return _url
}
