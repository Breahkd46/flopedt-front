


const JWTService = {
    ID_TOKEN_KEY: "token",
    getToken()  {
        return window.localStorage.getItem(this.ID_TOKEN_KEY);
    },

    saveToken(token) {
        window.localStorage.setItem(this.ID_TOKEN_KEY, token);
    },

    destroyToken() {
        window.localStorage.removeItem(this.ID_TOKEN_KEY);
    },
}

export default JWTService
