import {AUTH_ACCESS} from "@/commons/auth.model";
import {URLS_NAME} from "@/router/urls.model";


export const TABS = [
    {
        displayName: 'Consulter',
        name: URLS_NAME.EDT,
        path: '/edt',
        authAccess: AUTH_ACCESS.NO_CONNECTION_REQUIRED
    },
    {
        displayName: 'Admin',
        name: URLS_NAME.ADMIN,
        path: '/admin',
        authAccess: AUTH_ACCESS.ADMIN_CONNECTION_REQUIRED
    }
]
