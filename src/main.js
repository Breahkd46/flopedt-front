import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from '@/router';
import vuetify from './plugins/vuetify';
import store from '@/store';
import EdtFilterPlugin from "@/commons/filters/edt.filters";

Vue.config.productionTip = false

Vue.use(VueRouter)
Vue.use(EdtFilterPlugin)

new Vue({
  store,
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
