import store from '@/store/index'

const isConnectedUserGuard = (to, from, next) => {
    if (isConnectedUser()) {
        next()
    } else {
        next({name: 'edt'})
    }
}
const isAdminUserGuard = (to, from, next) => {
    if (isAdminUser()) {
        next()
    } else {
        next({name: 'edt'})
    }
}

const isNotConnectedUserGuard = (to, from, next) => {
    if (!isConnectedUser()) {
        next()
    } else {
        next({name: 'edt'})
    }
}

const goodDepartmentGuard = (to, from, next) => {
    console.log('goodDepartmentGuard')
    if (store.getters.getDepartments.find(value => value.abbrev === to.params.department) !== undefined) {
        next()
    }
    next(false)
}

const isConnectedUser = () => {
    return store.state.auth.userConnectionStatus;
}

const isAdminUser = () => {
    return isConnectedUser() && store.state.auth.userAdmin;
}

export default {
    isConnectedUserGuard,
    isAdminUserGuard,
    isNotConnectedUserGuard,
    goodDepartmentGuard,

    isConnectedUser,
    isAdminUser
}
