import DepartmentChoice from "@/features/department/views/DepartmentChoice";
import DepartmentView from "@/features/edt/views/DepartmentView";
import Admin from "@/features/admin/components/Admin";
import Login from '@/features/login/views/Login';
import VueRouter from 'vue-router'
import guards from './guards/auth'
import {URLS_NAME} from "@/router/urls.model";
import store from '@/store'
import {CHANGE_TAB} from "@/store/actions.type";

const router = new VueRouter({
    routes: [
        {
            path: '/edt',
            component: DepartmentChoice,
            name: URLS_NAME.EDT,
        },
        {
            path: '/edt/:department',
            component: DepartmentView,
            name: URLS_NAME.EDT_DEPARTMENT,
            // beforeEnter: guards.goodDepartmentGuard
        },
        {
            path: '/admin',
            component: Admin,
            name: URLS_NAME.ADMIN,
            beforeEnter: guards.isAdminUserGuard
        },
        {
            path: '/login',
            component: Login,
            name: URLS_NAME.LOGIN,
            beforeEnter: guards.isNotConnectedUserGuard
        },
        {
            path: '',
            redirect: '/edt'
        }
    ]
})
router.afterEach((to,) => {
    store.dispatch(CHANGE_TAB, to.name)
})



export default router
