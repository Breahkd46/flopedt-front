import {API_URL} from "@/commons/config";
import {url} from "@/commons/http.service";

export const DAYS = ['m', 'tu', 'w', 'th', 'f', 'sa', 'su']

const EdtService = {
    getWeekYears: () => {
        return new Promise((resolve, reject) => {
            fetch(API_URL + 'api/fetch/week/all/')
                .then(response => response.json())
                .then(response => {
                    resolve(response)
                }).catch(reason => {
                console.error(reason)
                reject(reason)
            })
        })
    },
    getCurrentWeek: () => {
        return new Promise((resolve, reject) => {
            fetch(API_URL + 'api/fetch/week/currentWeek/')
                .then(response => response.json())
                .then(response => {
                    resolve(response)
                }).catch(reason => {
                    console.error(reason)
                    reject(reason)
            })
        })
    },
    getTimeSettings(department) {
        return new Promise((resolve, reject) => {
            fetch(url(API_URL + 'api/base/timesettings/', {department: department}))
                .then(response => response.json())
                .then(response => {
                    resolve(response)
                }).catch(reason => {
                console.error(reason)
                reject(reason)
            })
        })
    },
    getScheduledCourse(department, week, year) {
        return new Promise((resolve, reject) => {
            fetch(url(API_URL + 'api/fetch/scheduledcourses/', {
                dept: department,
                week: week,
                year: year
            })).then(value => value.json())
                .then(value => resolve(value))
                .catch(reason => reject(reason))
        })
    },
    getGroupsTree(department) {
        return new Promise((resolve, reject) => {
            fetch(url(API_URL + 'api/groups/group/tree/', {
                dept: department,
            })).then(value => value.json())
                .then(value => resolve(value))
                .catch(reason => reject(reason))
        })
    },
    getRooms(department) {
        return new Promise((resolve, reject) => {
            fetch(url(API_URL + 'api/rooms/names/', {
                dept: department,
            })).then(value => value.json())
                .then(value => resolve(value))
                .catch(reason => reject(reason))
        })
    },
    getWeekDays(department, week, year) {
        return new Promise((resolve, reject) => {
            fetch(url(API_URL + 'api/fetch/week/days/', {
                week: week,
                year: year,
                dept: department,
            })).then(value => value.json())
                .then(value => resolve(value))
                .catch(reason => reject(reason))
        })
    }
}

export default EdtService
