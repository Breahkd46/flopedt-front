import {LOGIN, LOGOUT} from "./actions.type";
import {PURGE_AUTH, SET_AUTH} from "./mutations.type";
import {API_URL} from "@/commons/config";
import JWTService from "@/commons/jwt.service";
import {AUTH_ACCESS} from "@/commons/auth.model";

const state = {
    username: '',
    userConnectionStatus: false,
    userAdmin: false
}

const mutations = {
    [SET_AUTH](state, username) {
        state.username = username;
        state.userConnectionStatus = true;
    },
    [PURGE_AUTH](state) {
        state.username = '';
        state.userConnectionStatus = false;
    }
}

const getters = {
    getUsername: (state) => {
        return state.username;
    },
    getConnectionStatus: (state) => {
        return state.userConnectionStatus;
    },
    getAuthorizationAccess: (state) => (auth) => {
        if (auth === AUTH_ACCESS.NO_CONNECTION_REQUIRED) return true
        if (auth === AUTH_ACCESS.BASE_USERCONNECTION_REQUIRED &&
            state.userConnectionStatus) return true
        return auth === AUTH_ACCESS.ADMIN_CONNECTION_REQUIRED &&
            state.userAdmin;

    }
}

const actions = {
    [LOGIN]({commit}, credential) {
        return new Promise((resolve, reject) => {
            const myHeaders = new Headers();
            myHeaders.append('Content-Type', 'application/json');
            myHeaders.append('Accept', 'application/json');
            // myHeaders.append('Access-Control-Allow-Origin', '*')
            fetch(API_URL + 'api/rest-auth/login/', {
                method: 'POST',
                headers: myHeaders,
                body: JSON.stringify({
                    username: credential.username,
                    password: credential.password,
                })
            }).then((response) => {
                if (response.status === 200) {
                    const data = response.json();
                    commit(SET_AUTH, credential.username)
                    JWTService.saveToken(data.key)
                    resolve(data);
                } else {
                    resolve(false);
                }
            }).catch(reason => {
                reject(reason)
            })
        })
    },
    [LOGOUT] ({commit}) {
        return new Promise((resolve, reject) => {

            fetch(API_URL + 'api/rest-auth/logout/').then(({data}) => {
                console.log(data);
                commit(PURGE_AUTH);
                JWTService.destroyToken();
                resolve(data);
            }).catch(reason => {
                reject(reason);
            })
        })
    }

}

export default {
    // namespaced: true,
    state,
    getters,
    actions,
    mutations
}
