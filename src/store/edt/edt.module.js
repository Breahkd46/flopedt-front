import {
    PURGE_CURRENTWEEK,
    PURGE_DEPARTMENTS,
    PURGE_TIMESETINGS,
    PURGE_WEEKYEAR,
    SET_CURRENTWEEK,
    SET_DEPARTMENT,
    SET_DEPARTMENTS,
    SET_GROUP_TREE,
    SET_GROUP_TREE_SELECTED, SET_ROOMS,
    SET_TIMESETINGS,
    SET_WEEKYEAR,
} from "@/store/edt/mutation.type";
import {
    INIT_DEPARTMENT,
    GET_CURRENTWEEK,
    GET_DEPARTMENTS,
    GET_WEEKYEAR,
    INIT_EDT,
    GET_TIMESETTING,
    INIT_WEEK_SERVICE,
    GET_GROUP_TREE, GET_ROOMS, INIT_ROOMS_FILTER
} from "@/store/edt/action.type";
import {API_URL} from "@/commons/config";
import EdtService from "@/services/edt.service";
import {setStateTree} from "@/commons/groups.service";


const state = {
    department: '',
    departments: [],
    departmentsStatus: false,
    currentWeek: {},
    weekYear: [],
    timeSettings: [],
    timeSettingStatus: false,
    groupTree: [],
    rooms: []
}

const mutations = {
    [SET_DEPARTMENT](state, department) {
        state.department = department;
    },
    [SET_DEPARTMENTS](state, departments) {
        state.departmentsStatus = true;
        state.departments = departments;
    },
    [PURGE_DEPARTMENTS](state) {
        state.departmentsStatus = false;
        state.departments = [];
    },
    [SET_WEEKYEAR](state, weeks) {
        state.weekYear = weeks
    },
    [PURGE_WEEKYEAR](state) {
        state.weekYear = []
    },
    [SET_CURRENTWEEK](state, week) {
        state.currentWeek = week
    },
    [PURGE_CURRENTWEEK](state) {
        state.currentWeek = {}
    },
    [SET_TIMESETINGS](state, timeSetting) {
        state.timeSettings = timeSetting
        state.timeSettingStatus = true
    },
    [PURGE_TIMESETINGS](state) {
        state.timeSettings = {}
        state.timeSettingStatus = false
    },
    [SET_GROUP_TREE](state, groups) {
        state.groupTree = groups
    },
    [SET_ROOMS](state, rooms) {
        state.rooms = rooms
    }
}

const getters = {
    getDepartment: (state) => {
        return state.department;
    },
    getDepartments: (state) => {
        return state.departments;
    },
    getCurrentWeek: (state) => {
        return state.currentWeek
    },
    getWeekYear: (state) => {
        return state.weekYear
    },
    getTimeSettings: (state) => {
        return state.timeSettings
    },
    getTimeSetting: (state) => (day) => {
        return state.timeSettings.find(time => time.days.includes(day))
    },
    getGroupTree: (state) => {
        return state.groupTree
    },
    getPromo: (state) => {
        return state.groupTree.map(value => value.promo)
    },
    getRooms: (state) => {
        return state.rooms
    }
}

const actions = {
    [GET_DEPARTMENTS]({commit, state}) {
        return new Promise((resolve, reject) => {
            if (state.departmentsStatus) {
                resolve(this.departments);
            } else {
                fetch(API_URL + 'api/base/departments/')
                    .then(response => response.json())
                    .then(response => {
                        commit(SET_DEPARTMENTS, response)
                        resolve(response)
                    }).catch(reason => {
                        console.error(reason)
                        reject(reason)
                })
            }
        })
    },
    [INIT_DEPARTMENT]({commit, dispatch, state}, department) {
        if (state.departments.length === 0 || state.departments.filter(value => value.abbrev === department)) {
            commit(SET_DEPARTMENT, department);
            dispatch(GET_TIMESETTING, department)
            dispatch(GET_GROUP_TREE, department)
            dispatch(GET_ROOMS, department)
            return department
        }
        return null
    },
    [GET_CURRENTWEEK]({commit, state}) {
        return new Promise((resolve, reject) => {
            if (state.currentWeekStatus) {
                resolve(state.currentWeek)
            } else {
                fetch(API_URL + 'api/fetch/week/currentWeek/')
                    .then(response => response.json())
                    .then(response => {
                        commit(SET_CURRENTWEEK, response)
                        resolve(response)
                    }).catch(reason => {
                    console.error(reason)
                    reject(reason)
                })
            }
        })
    },
    [GET_WEEKYEAR]({commit}) {
        return new Promise((resolve, reject) => {
            fetch(API_URL + 'api/fetch/week/all/')
                .then(response => response.json())
                .then(response => {
                    commit(SET_WEEKYEAR, response)
                    resolve(response)
                }).catch(reason => {
                console.error(reason)
                reject(reason)
            })
        })
    },
    [GET_TIMESETTING]({commit, state}, department) {
        return new Promise((resolve, reject) => {
            if (state.timeSettingStatus) {
                resolve(state.timeSettings)
            } else {
                EdtService.getTimeSettings(department).then((value) => {
                    commit(SET_TIMESETINGS, value)
                    resolve(value)
                }).catch(reason => {
                    reject(reason)
                })
            }
        })
    },
    [INIT_WEEK_SERVICE]({dispatch}) {
        return Promise.all([
            dispatch(GET_WEEKYEAR),
            dispatch(GET_CURRENTWEEK)
        ])
    },
    [INIT_EDT]({dispatch}) {
        return Promise.all([
            dispatch(GET_DEPARTMENTS),
            dispatch(GET_WEEKYEAR),
            dispatch(GET_CURRENTWEEK)
        ])
    },
    [GET_GROUP_TREE]({commit}, department) {
        return new Promise((resolve, reject) => {
            EdtService.getGroupsTree(department).then(value => {
                commit(SET_GROUP_TREE, value)
                const tree = {}
                value.forEach(group => {
                    tree[group.promo] = setStateTree(group, true)
                })
                commit(SET_GROUP_TREE_SELECTED, tree)
                resolve(value)
            }).catch(reason => reject(reason))
        })
    },
    [GET_ROOMS]({commit, dispatch}, department) {
        return new Promise((resolve, reject) => {
            EdtService.getRooms(department).then(value => {
                commit(SET_ROOMS, value)
                dispatch(INIT_ROOMS_FILTER)
                resolve(value)
            }).catch(reason => reject(reason))
        })
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
