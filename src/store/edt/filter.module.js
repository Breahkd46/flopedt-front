import {SET_DISPLAY_MODE, SET_ROOMS_FILTER} from "@/store/edt/mutation.type";
import {DISPLAY_MODE} from "@/commons/display.model";
import {SET_GROUP_TREE_SELECTED} from "./mutation.type";
import {INIT_ROOMS_FILTER, UPDATE_GROUP_TREE_SELECTED} from "./action.type";
import {buildGroupSelected} from "@/commons/groups.service";


const state = {
    groupTreeSelected: {},
    displayMode: DISPLAY_MODE.SLOT,
    roomsFilter: []
}

const mutations = {
    [SET_DISPLAY_MODE](state, display) {
        state.displayMode = display
    },
    [SET_GROUP_TREE_SELECTED](state, groups) {
        state.groupTreeSelected = groups
    },
    [SET_ROOMS_FILTER](state, rooms) {
        state.roomsFilter = rooms
    }
}

const getters = {
    getDisplayMode: (state) => {
        return state.displayMode
    },
    getGroupTreeSelected: (state) => {
        return state.groupTreeSelected
    },
    getRoomsFilter: (state) => {
        return state.roomsFilter
    },
    getRoomIsSelected: (state) => (room) => {
        if (state.roomsFilter.some(r => r.selected)) {
            return state.roomsFilter.find(roomF => roomF.name === room).selected
        } else {
            return true
        }

    }
}

const actions = {
    [UPDATE_GROUP_TREE_SELECTED]({commit, getters}, updateItem) {
        new Promise((resolve, reject) => {
            if (!getters.getPromo.includes(updateItem.promo)) {
                reject()
            } else {
                const tree = buildGroupSelected(getters.getPromo, getters.getGroupTreeSelected)
                tree[updateItem.promo] = updateItem.group
                commit(SET_GROUP_TREE_SELECTED, tree)
                resolve()
            }
        })
    },
    [INIT_ROOMS_FILTER]({commit, getters}) {
        commit(SET_ROOMS_FILTER, getters.getRooms.map(room => {
                return {name: room.name, selected: false}
            })
        )
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
