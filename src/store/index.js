import Vuex from 'vuex'
import Vue from "vue";
import auth from './auth.module'
import topBar from './topbar.module'
import edt from './edt/edt.module'
import edtFilters from './edt/filter.module'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    topBar,
    edt,
    edtFilters
  }
})
