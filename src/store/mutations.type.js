// Auth
export const SET_AUTH = 'SET_AUTH';
export const PURGE_AUTH = 'PURGE_AUTH';

// TopBAR
export const SET_TAB = 'SET_TAB';
export const PURGE_TAB = 'PURGE_TAB';
