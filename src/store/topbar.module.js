import {PURGE_TAB, SET_TAB} from "@/store/mutations.type";
import {CHANGE_TAB} from "@/store/actions.type";

const state = {
    selectedTab: ''
}

const mutations = {
    [SET_TAB](state, tab) {
        state.selectedTab = tab;
    },
    [PURGE_TAB](state) {
        state.selectedTab = '';
    }
}

const getters = {
    selectedTab: (state) => {
        return state.selectedTab
    }
}

const actions = {
    [CHANGE_TAB]({commit}, tab) {
        commit(SET_TAB, tab)
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}
